package pl.codehussar.payment.model;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SplitMoneyRequest {

  private List<String> userNames;
  private Double moneyToSplit;

}
