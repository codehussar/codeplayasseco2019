package pl.codehussar.payment.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Account {

  private String bankName;
  private String accountBalance;
  private String currency;
  private String accountNumber;
}
