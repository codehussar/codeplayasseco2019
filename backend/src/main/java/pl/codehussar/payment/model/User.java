package pl.codehussar.payment.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
  String userName;
  List<Account> accounts;
}
