package pl.codehussar.payment.api;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.codehussar.payment.model.Account;
import pl.codehussar.payment.model.SplitMoneyRequest;
import pl.codehussar.payment.service.AccountService;

@CrossOrigin
@Slf4j
@RequiredArgsConstructor
@Service
@RequestMapping("/payment")
public class PaymentController {

  private final AccountService accountService;


  @CrossOrigin
  @GetMapping("/accounts/{userName}")
  public ResponseEntity<List<Account>> getMyAccounts(@RequestParam String userName) {
    log.info("Request");
    List<Account> accounts = accountService.getAccounts(userName);
    if(accounts == null){
      return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    } else return new ResponseEntity<>(accounts, HttpStatus.OK );
  }

  @CrossOrigin
  @PostMapping("/splitmoney")
  public ResponseEntity splitMoney(@RequestBody SplitMoneyRequest splitMoneyRequest) {
    log.info("Request");
    accountService.setUsersForSplit(splitMoneyRequest);
    return new  ResponseEntity(HttpStatus.OK);
  }

  @CrossOrigin
  @GetMapping("/getMyLongs")
  public ResponseEntity<String> getMyLongs(String userName) {
    log.info("Request");
    return new ResponseEntity<>(accountService.getMyLongs(userName), HttpStatus.OK);
  }
}
