package pl.codehussar.payment.repository;

import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.codehussar.payment.model.Account;

@Service
@RequiredArgsConstructor
public class AccountMemoryRepository implements AccountRepository {

  private Map<String, Account> ACCOUNTS = new HashMap<>();

  @Override
  public Account findByAccountNumber(String accountNumber) {
    return ACCOUNTS.get(accountNumber);
  }

  @Override
  public void add(Account account, String accountNumber) {
    ACCOUNTS.put(accountNumber, account);
  }

  @Override
  public void delete(String accountNumber) {
    ACCOUNTS.remove(accountNumber);
  }
}
