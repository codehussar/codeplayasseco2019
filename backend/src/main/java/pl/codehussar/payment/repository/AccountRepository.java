package pl.codehussar.payment.repository;

import pl.codehussar.payment.model.Account;

public interface AccountRepository {

  Account findByAccountNumber(String accountNumber);

  void add(Account account, String accountNumber);


  void delete(String accountNumber);
}
