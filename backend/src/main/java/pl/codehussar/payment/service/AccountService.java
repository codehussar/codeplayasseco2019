package pl.codehussar.payment.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.codehussar.payment.model.Account;
import pl.codehussar.payment.model.SplitMoneyRequest;
import pl.codehussar.payment.model.User;
import pl.codehussar.payment.repository.AccountRepository;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

  private Map<String, User> users;
  private List<String> userNamesForSplit;
  private double moneyToPay;

  private final AccountRepository accountRepository;

  public List<Account> getAccounts(String userName) {
    User user = users.get(userName);
    if(user == null){
      return null;
    }
    return user.getAccounts();
  }

  public void setUsersForSplit(SplitMoneyRequest usersForSplit) {

    userNamesForSplit = new ArrayList<>();
    userNamesForSplit.addAll(usersForSplit.getUserNames());
    userNamesForSplit.forEach(x -> log.info(x));
    for(String x : userNamesForSplit){
      log.info(x);
    }
    moneyToPay = usersForSplit.getMoneyToSplit() / usersForSplit.getUserNames().size();
  }

  @PostConstruct
  private void addTestData(){
    users = new HashMap<>();

    userNamesForSplit = new ArrayList<>();

    User user = new User();
    user.setUserName("admin");
    user.setAccounts(new ArrayList<>());

    Account account1 = new Account();
    account1.setAccountBalance("12350");
    account1.setAccountNumber("25249000053735543965529406");
    account1.setBankName("Pekao S.A.");
    account1.setCurrency("PLN");

    Account account2 = new Account();
    account2.setAccountBalance("23450");
    account2.setAccountNumber("20249000052597385509567123");
    account2.setBankName("Pekao BP");
    account2.setCurrency("PLN");

    Account account3 = new Account();
    account3.setAccountBalance("23456");
    account3.setAccountNumber("72249000054046052214997442");
    account3.setBankName("Millenium");
    account3.setCurrency("EUR");

    user.getAccounts().add(account1);
    user.getAccounts().add(account2);
    user.getAccounts().add(account3);
    users.put(user.getUserName(), user);
  }

  public String getMyLongs(String userName) {
    if(userNamesForSplit.contains(userName)){
      int xy = 0;
      for(String x : userNamesForSplit){
        if(x.equals(userName)){
          xy = userNamesForSplit.indexOf(x);
        }
      }
      userNamesForSplit.remove(xy);
      userNamesForSplit.forEach(x -> log.info(x));

      return Double.toString(moneyToPay);
    } else {
      return "0";
    }
  }
}
