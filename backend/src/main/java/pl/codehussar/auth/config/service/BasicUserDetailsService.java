package pl.codehussar.auth.config.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.codehussar.auth.config.model.BasicUserDetails;
import pl.codehussar.auth.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class BasicUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    BasicUserDetails res = userRepository.findByUserame(username);
    if (res == null) {
      throw new UsernameNotFoundException("User " + username + " not found");
    }
    return res;
  }
}
