package pl.codehussar.auth;

import java.util.ArrayList;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.codehussar.auth.config.model.BasicUserDetails;
import pl.codehussar.auth.repository.UserRepository;

@Component
@RequiredArgsConstructor
public class PostConstructService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  @PostConstruct
  private void init() {
    userRepository.addUser(new BasicUserDetails(Arrays.asList(new SimpleGrantedAuthority("ADMIN")), passwordEncoder.encode("admin"), "admin", true, true, true, true));
    userRepository.addUser(new BasicUserDetails(Arrays.asList(new SimpleGrantedAuthority("USER")), passwordEncoder.encode("Password2"), "User2", true, true, true, true));
    userRepository.addUser(new BasicUserDetails(Arrays.asList(new SimpleGrantedAuthority("ADMIN"), new SimpleGrantedAuthority("USER")), passwordEncoder.encode("Password3"), "User3", true, true, true, true));
    userRepository.addUser(new BasicUserDetails(Arrays.asList(new SimpleGrantedAuthority("FULL_ACCESS")), passwordEncoder.encode("Password4"), "User4", true, true, true, true));
    userRepository.addUser(new BasicUserDetails(new ArrayList<>(), passwordEncoder.encode("Password5"), "User5", true, true, true, true));
  }
}
