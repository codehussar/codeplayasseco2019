package pl.codehussar.auth.repository;

import pl.codehussar.auth.config.model.BasicUserDetails;

public interface UserRepository {

  void addUser(BasicUserDetails userDetails);

  BasicUserDetails findByUserame(String username);

}
