package pl.codehussar.auth.repository;

import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.codehussar.auth.config.model.BasicUserDetails;

@Service
@RequiredArgsConstructor
public class UserMemoryRepository implements UserRepository {



  private Map<String, BasicUserDetails> USERS = new HashMap<>();

  @Override
  public void addUser( BasicUserDetails userDetails) {
    USERS.put(userDetails.getUsername(), userDetails);
  }

  @Override
  public BasicUserDetails findByUserame(String username) {
    return USERS.get(username);
  }
}
