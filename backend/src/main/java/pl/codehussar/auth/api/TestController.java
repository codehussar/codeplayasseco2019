package pl.codehussar.auth.api;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping(path = "/test")
public class TestController {


  @PreAuthorize("permitAll()")
  @GetMapping({"/openService"})
  public String openService() {
    return "openService success";
  }

  @PreAuthorize("isAuthenticated()")
  @GetMapping({"/authenticatedService"})
  public String authenticatedService() {
    return "authenticatedService success";
  }


  @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('FULL_ACCESS')")
  @GetMapping({"/adminService"})
  public String adminService() {
    return "adminService success";
  }

  @PreAuthorize("hasAuthority('USER') or hasAuthority('FULL_ACCESS')")
  @GetMapping({"/userService"})
  public String userService() {
    return "userService success";
  }

  @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER') or hasAuthority('FULL_ACCESS')")
  @GetMapping({"/commonService"})
  public String commonService() {
    return "commonService success";
  }

  @GetMapping({"/unexpectedService"})
  public String unexpectedService() {
    return "unexpectedService success";
  }
}
