package pl.codehussar.auth.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.codehussar.auth.model.AuthData;
import pl.codehussar.auth.service.AuthService;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {

  private final AuthService authService;

  @PreAuthorize("permitAll()")
  @PostMapping("/login")
  public ResponseEntity login(@RequestBody AuthData authData) {
    authService.login(authData);
    return new ResponseEntity(HttpStatus.OK);
  }

  @PreAuthorize("isAuthenticated()")
  @GetMapping("/logout")
  public void logout(HttpServletRequest request, HttpServletResponse response) {
    authService.logout(request, response);
  }
}
