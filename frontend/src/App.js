import React from 'react';
// import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory 
} from "react-router-dom";
import styled from 'styled-components'


import { Login } from './components/Login'
import { Stats } from './components/Stats'

import { Split } from './components/Split'
import { History } from './components/History'
import { Card } from './components/Card'
import axios from 'axios';
import { Approve } from './components/Approve';

const url = 'http://192.168.137.92:8080';
const userName = "64aw6d8awd4awg4awga8wd84awd48"


function App() {
  let history = 2 // useHistory();

  localStorage.setItem('login', '');

  function chekForNotif() {
    setTimeout(() => {
      console.log(window.localStorage.login)
      axios.get(`${url}/payment/getMyLongs?userName=${window.localStorage.login ? window.localStorage.login : 'dawdawdfhawgjkasfklasdklasdnawcbjjawd'}`, {
        method: 'GET',
        mode: 'cors',
        headers: { 'Access-Control-Allow-Origin': true },
      })
        .then(function (response) {
          console.log(response.data);
          chekForNotif()
          if (response.data !== 0) {
            history.push('/approve')
            console.log('inside')
          }
        })
        .catch((e) => { console.error(e) })
    }, 1000);
  }

  chekForNotif()

  return (
    <Router>
      <BackgroundCity />
      <BackgroundBlue />
      <Switch>
        <Route exact path="/"><Login /> </Route>
        <Route exact path="/stats"><Stats /> </Route>
        <Route exact path="/history"> <History /> </Route>
        <Route exact path="/card"><Card /></Route>
        <Route exact path="/split"> <Split /> </Route>
        <Route exact path="/approve"> <Approve /> </Route>
      </Switch>


    </Router>
  );
}

export default App;



const BackgroundCity = styled.div`
background: url('/assets/backgroundCity.svg') no-repeat;
  background-size: cover;
  width: 100vw;
  height: 100vh;
  position: absolute;
  z-index: -5000;
`


const BackgroundBlue = styled.div`
background: url('/assets/backgroundBlue.svg') no-repeat ;
background-size: cover;
width: 100vw;
height: 100vh;
position: absolute;
z-index: -4000;
background-position-y: -400px;
opacity: 0.8;
`