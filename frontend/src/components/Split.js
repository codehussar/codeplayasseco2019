import React, { useState } from 'react'

import styled from "styled-components";

import { MenuButtons } from "./MenuButtons";
import { Checker } from "./Checker";
import { Col, Row, Container } from 'reactstrap';
import { Link } from 'react-router-dom'
import axios from 'axios';


const url = 'http://192.168.137.92:8080';

function Split(params) {

    var state = [0, 0, 0, 0];
    const [count, setCount] = useState(1);

    const setState = (id) => {
        console.log(id)
        if (state[id] == 0) {
            state[id] = 1;
        } else {
            state[id] = 0;
        }
    }

    const PayMooron = () => {
        console.log('PAY')
        console.log(params.amount)
        console.log(state)
        let tab = [];
        for (let index = 0; index < state.length; index++) {
            const element = state[index];
            if (element) {
                let temp = index + 1
                tab.push(`demo${temp}`)
            }
        }
        console.log(tab)
        axios.post(`${url}/payment/splitmoney`, { moneyToSplit: params.amount, userNames: tab },
            {
                headers: { 'Access-Control-Allow-Origin': true },
            })
            .then(function (response) {
                // console.log(request)
                console.log(response.data);
                // chekForNotif()
            })
            .catch((e) => { console.error(e) })

    }




    return (
        <AppCard>
            <MenuButtons />
            <br />
            <Card2>
                <span style={{ fontSize: '1.5rem' }}>
                    Do podziału: {params.money || 50}
                </span>
            </Card2>
            <br />
            <Card onClick={() => { setState(0) }}>
                <Checker name="demo1" />
            </Card>

            <Card onClick={() => { setState(1) }}>
                <Checker name="demo2" />
            </Card>

            <Card onClick={() => { setState(2) }}>
                <Checker name="demo3" />
            </Card>

            <Card onClick={() => { setState(3) }}>
                <Checker name="demo4" />
            </Card>

            <Butt onClick={PayMooron} to="/card">
                ZAPŁAĆ
            </Butt>

        </AppCard>
    )
}

export { Split }

const Butt = styled(Link)`
display:block;
color: white;

margin-top:20px;
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
text-align: center;
color: #FFFFFF;
background: #21B630;
font-size: 4vw;
text-align:center;
padding-top:10px;
padding-bottom:10px;
box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.8);
`

const Card = styled.div`
padding:10px
margin:10px;
border:solid 1px #a6a6a6;
border-radius: 10px;
background: #FDF7F7;
box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.8);
font-size:2rem;
`

const Card2 = styled.div`
border:solid 1px #a6a6a6;
border-radius: 10px;
background: #FDF7F7;
margin-left:5px;
margin-right:5px;
box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.8);
`
const AppCard = styled.div`
    text-align: center;
    font-size: 0.5rem;
    margin: 20vh 2.5vw 0;
    height: 75vh;
    position: absolute;
    width: 95vw;
    background: #FBFAFA;
    border: 1px solid #000000;
    box-sizing: border-box;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
`