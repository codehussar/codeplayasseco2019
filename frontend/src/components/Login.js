import React, { useState } from 'react'

import { Redirect } from 'react-router-dom'

import styled from 'styled-components'
import axios from 'axios';


const url = 'http://192.168.137.92:8080';

function Login() {

    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const [loged, toLogin] = useState(false)

    
    const sumbitLogin = (e) => {
        // console.log(window.localStorage.login)
        if (e.keyCode === 13) {
            
            localStorage.setItem('login', login);
            if (window.localStorage.login) toLogin(true)
            console.log({
                login: login,
                password: password
            })
            
            localStorage.setItem('login', login);
            
        }
    }
    
        if (loged) return (<Redirect to="/card"></Redirect>)



    return (
        <>
            {/* Login Component */}
            <LoginContainer>
                <Desc>Login:</Desc>
                <Spacer>
                    <Invisible onSubmit={() => sumbitLogin()} value={login} onKeyDown={sumbitLogin} onChange={(e) => setLogin(e.target.value)} ></Invisible>
                    <MyImg src="./assets/loginWhite.png" />
                </Spacer>
                <Desc>Password:</Desc>
                <div>
                    <Invisible type='password' onSubmit={() => sumbitLogin()} value={password} onKeyDown={sumbitLogin} onChange={(e) => setPassword(e.target.value)} ></Invisible>
                    <MyImg src="./assets/login.png" />
                </div>
            </LoginContainer>
            {/* <span tooltip="">hintIcon</span> */}
            {/* <button onClick={check}>Sprawdx</button> */}
        </>
    )
}

export { Login }

const LoginContainer = styled.div`
margin-top:40vh;
position:absolute;
`
const Spacer = styled.div`
margin-bottom:5vh
`

const Desc = styled.span`
color:white
font-size:2rem
`

const MyImg = styled.img`
height:4rem
width:70vw
maring-bottom:10vh
`

const Invisible = styled.input`
font-size:2.5rem
background-color:transparent;
position:absolute
border:none;
:focus{
    border:none;
    outline: none;
}
`