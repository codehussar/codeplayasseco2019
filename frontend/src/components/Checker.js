import React, { useState } from 'react'

import styled from "styled-components";

import { MenuButtons } from "./MenuButtons";
import { Col,Row, Container } from 'reactstrap';

function Checker(props){

    const [status, setStatus]=useState(0);

    if(status==0){
        return(
            <>
                    <Container>
                        <Row>
                            <Col xs="3">
                                <Icon>
                                   <img className="img-fluid" src='./assets/person.png'></img>
                                </Icon>
                            </Col>
                            <Col xs="5">
                                <Name>
                                {props.name}
                                </Name>
                            </Col>
                            <Col xs="3">
                                 <CheckBox onClick={() => {setStatus(1)}} fontSize="large">
                                      <img className="img-fluid" src='./assets/no.png'></img>
                                 </CheckBox>
                            </Col>
                        </Row>
                    </Container>
            </>
        );
    }else{
        return(
            <>
                    <Container>
                        <Row>
                            <Col xs="3">
                                <Icon>
                                   <img className="img-fluid" src='./assets/person.png'></img>
                                </Icon>
                            </Col>
                            <Col xs="5">
                                <Name>
                                {props.name}
                                </Name>
                            </Col>
                            <Col xs="3">
                                 <CheckBox onClick={() => {setStatus(0)}} fontSize="large">
                                      <img className="img-fluid" src='./assets/yes.png'></img>
                                 </CheckBox>
                            </Col>
                        </Row>
                    </Container>
            </>
        );
    }
}

export {Checker};

const CheckBox=styled.div`
`

const Icon = styled.div`
padding:0px;
padding-left:5px;
padding-right:5px;
`
const Name=styled.h4`
font-size:1rem;
padding-top:15px;
`