
import React, { useState } from 'react'

import { Redirect } from "react-router-dom";

import styled from 'styled-components'

import CreditCardIcon from '@material-ui/icons/CreditCard';
import DonutSmallIcon from '@material-ui/icons/DonutSmall';
import HistoryIcon from '@material-ui/icons/History';


const MenuButtons = () => {
    const [redirect, goTo] = useState('');

    if (redirect && !document.location.pathname.includes(redirect)) {
        return (<Redirect to={redirect} />)

    }
    return (
        <>
            <MenuContainer>
                <MenuButton onClick={() => { if (redirect !== 'stats') goTo('stats') }}>
                    <IconDiv>
                        <DonutSmallIcon fontSize="large" />
                    </IconDiv>
                    Analiza
     </MenuButton>
                <MenuButton onClick={() => { if (redirect !== 'history') goTo('history') }}>
                    <IconDiv>
                        <HistoryIcon fontSize="large" />
                    </IconDiv>
                    Historia
     </MenuButton>
                <MenuButton onClick={() => { if (redirect !== 'card') goTo('card') }}>
                    <IconDiv>
                        <CreditCardIcon fontSize="large" />
                    </IconDiv>
                    Rachunki
     </MenuButton>
            </MenuContainer>
        </>
    )
}


export { MenuButtons }


const IconDiv = styled.div`
    font-size:3vw;
    margin: 2vw 0 0;
    `


const MenuContainer = styled.div`
    color:#666;
    margin: -10vw 0 0 15vw;
    display:flex;
    flex-direction:row;
    width: max-content;
    // position:absolute
    z-index: 4;
    font-size:4vw

`

const MenuButton = styled.div`
    text-align:center;
    margin-right:4px;
    width: 21vw;
    height: 21vw;
    background-color:#FFF;
    border: solid 2px #a0a0a0 ;
    border-radius:10px;
    `
