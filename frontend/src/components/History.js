import React, { useState } from 'react'
import styled from 'styled-components';
import { Col, Row, Container } from 'reactstrap';

import { Split } from './Split'
import { MenuButtons } from "./MenuButtons";

function History() {
    const [value, clicked] = useState(0)
    const handleClick = (amount) => {
        clicked(amount);
    }

    if (value) {
        console.log(value)
        return (<Split amount={value} />)
    }


    return (
        <div className="AppCard">
            <MenuButtons />
            <div className="TextBox">
                <Container>
                    <Row>
                        <Col>

                            <Row>
                                <Line>
                                    <Con>
                                        <Col xs="12">
                                            <Row>
                                                <Col xs="3">
                                                    <Icon>
                                                        <img className="img-fluid" src='./assets/Group.png'></img>
                                                    </Icon>
                                                </Col>
                                                <Col xs="5">
                                                    <Description>
                                                        <BankName>
                                                            12.10.2019
                                                </BankName>
                                                        <Saldo>
                                                            50 PLN
                                                </Saldo>
                                                        <Account>
                                                            SKLEP ŻABKA
                                                </Account>
                                                    </Description>
                                                </Col>
                                                <Col xs="4">
                                                    <Butt onClick={() => handleClick(20)}>
                                                        Podziel
                                            </Butt>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Con>
                                </Line>
                            </Row>

                            <Row>
                                <Line>
                                    <Con>
                                        <Col xs="12">
                                            <Row>
                                                <Col xs="3">
                                                    <Icon>
                                                        <img className="img-fluid" src='./assets/Flat.png'></img>
                                                    </Icon>
                                                </Col>
                                                <Col xs="5">
                                                    <Description>
                                                        <BankName>
                                                            12.10.2019
                                                </BankName>
                                                        <Saldo>
                                                            150 PLN
                                                </Saldo>
                                                        <Account>
                                                            Starbucks Cafe
                                                </Account>
                                                    </Description>
                                                </Col>
                                                <Col xs="4">
                                                    <Butt onClick={() => handleClick(20)}>
                                                        Podziel
                                            </Butt>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Con>
                                </Line>
                            </Row>


                            <Row>
                                <Line>
                                    <Con>
                                        <Col xs="12">
                                            <Row>
                                                <Col xs="3">
                                                    <Icon>
                                                        <img className="img-fluid" src='./assets/Vector.png'></img>
                                                    </Icon>
                                                </Col>
                                                <Col xs="5">
                                                    <Description>
                                                        <BankName>
                                                            10.10.2019
                                                </BankName>
                                                        <Saldo>
                                                            10 PLN
                                                </Saldo>
                                                        <Account>
                                                            Mechanicy24.pl
                                                </Account>
                                                    </Description>
                                                </Col>
                                                <Col xs="4">
                                                    <Butt onClick={() => handleClick(20)}>
                                                        Podziel
                                            </Butt>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Con>
                                </Line>
                            </Row>


                            <Row>
                                <Line>
                                    <Con>
                                        <Col xs="12">
                                            <Row>
                                                <Col xs="3">
                                                    <Icon>
                                                        <img className="img-fluid" src='./assets/Group.png'></img>
                                                    </Icon>
                                                </Col>
                                                <Col xs="5">
                                                    <Description>
                                                        <BankName>
                                                            09.10.2019
                                                </BankName>
                                                        <Saldo>
                                                            10 PLN
                                                </Saldo>
                                                        <Account>
                                                            SKLEP ŻABKA
                                                </Account>
                                                    </Description>
                                                </Col>
                                                <Col xs="4">
                                                    <Butt onClick={() => handleClick(20)}>
                                                        Podziel
                                            </Butt>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Con>
                                </Line>
                            </Row>


                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export { History }

const Line = styled.div`
            width:80vw
            margin-left:8vw;
            margin-bottom:5vh;
            background: #F8F8F8;
            box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.8);
            `
const Con = styled.div`
            `

const Butt = styled.div`
            
            margin-top:20px;
            font-family: Roboto Condensed;
            font-style: normal;
            font-weight: normal;
            text-align: center;
            color: #FFFFFF;
            background: #21B630;
            font-size: 4vw;
            text-align:center;
            padding-top:10px;
            padding-bottom:10px;
            box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.8);
            `

const Icon = styled.div`
            padding:10px;
            padding-top:20px;
            `

const BankName = styled.h3`
            
            font-family: Roboto Condensed;
            font-style: normal;
            font-weight: normal;
            font-size: 3vw;
            text-align: center;
            color: #878787;
            
            
            `

const Description = styled.div`
            margin:10px;
            `


const Account = styled.h4`
            font-family: Roboto Condensed;
            font-style: normal;
            font-weight: normal;
            font-size: 3vw;
            text-align: center;
            
            color: #878787;
            
            
            `
const Saldo = styled.h3`
            font-family: Roboto Condensed;
            font-style: normal;
            font-weight: normal;
            font-size: 4vw;
            text-align: center;
            
            color: #f44336;
`