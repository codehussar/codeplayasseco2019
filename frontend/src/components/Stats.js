import React from 'react'

// import { Link } from "react-router-dom";

import styled from 'styled-components'

import {MenuButtons} from './MenuButtons'
import {Col} from "reactstrap";


function Stats() {

    return (
        <>
            <AppCard>
                <MenuButtons>
                </MenuButtons>
                {/* <Link to="/create" className="noselect button">Stwórz zbiórkę</Link> */}
                {/* <Link to="/pay" className="noselect button">Opłać zbiórkę</Link> */}
                <TitleContainer>
                    <p>Twoje finanse</p>
                    <Line>
                        <BankName>Optymalizacja</BankName>
                        <Saldo >+355,67 PLN</Saldo>
                        <Account>KORZYŚĆ Z OPTYMALIZACJI KOSZTÓW</Account>
                    </Line>
                    <Line>
                        <BankName>Propozycja lokaty</BankName>
                        <Saldo>10 000 PLN</Saldo>
                        <Account>LOKATA 14 MIESIĘCZNA w banku Pekao S.A.</Account>
                    </Line>
                </TitleContainer>
            </AppCard>

        </>
    )
}

export {Stats}

const AppCard = styled.div`
    text-align: center;
    font-size: 1rem;
    margin: 10vh 2.5vw 0;
    height: 75vh;
    position: absolute;
    width: 95vw;
    background: #FBFAFA;
    border: 1px solid #000000;
    box-sizing: border-box;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
`

const TitleContainer = styled.div`
    color:#666;
    font-size:5vw
    margin: 5vw 0 0 0;
`

const Line = styled.div`
            width:80vw
            margin-left:8vw;
            margin-bottom:5vh;
            background: #F8F8F8;
            box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.8);
            `

const GreenMoney = styled.p`
    font-family: Roboto Condensed;
    font-style: normal;
    font-weight: normal;
    font-size: 7vw;
    line-height: 84px;
    text-align: center;
    color: #0FBC00;
`

const Saldo = styled.h3`
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
font-size: 7vw;
text-align: center;
color: #21B630;
`

const Account = styled.h4`
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
font-size: 5vw;
text-align: center;

color: #878787;
`


const BankName = styled.h3`

font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
font-size: 7vw;
text-align: center;
color: #000000;
`
