import React from 'react'
import styled from 'styled-components';
import { Col, Row, Container } from 'reactstrap';

import { MenuButtons } from './MenuButtons'

function Card() {
    return (
        <AppCard>
            <MenuButtons />
            <Container>
                <Row>
                    <Col xs="12">
                        <BankCard>
                            <Row>
                                <Col xs="1">
                                </Col>
                                <Col xs="4">
                                    <BankLogo>
                                        <img alt="" className="img-fluid" src='./assets/pkologo.png'></img>
                                    </BankLogo>
                                </Col>
                                <Col xs="6">
                                    <Bankdata>
                                        <BankName>PKO BP</BankName>
                                        <Account>12 3245 3245 4356</Account>
                                        <Saldo>123,34 PLN</Saldo>
                                    </Bankdata>
                                </Col>
                            </Row>
                        </BankCard>
                    </Col>

                    <Col xs="12">
                        <BankCard>
                            <Row>
                                <Col xs="1">
                                </Col>
                                <Col xs="4">
                                    <BankLogo>
                                        <img alt="" className="img-fluid" src='./assets/pekao.png'></img>
                                    </BankLogo>
                                </Col>
                                <Col xs="6">
                                    <Bankdata>
                                        <BankName>Pekao</BankName>
                                        <Account>12 3245 3245 4356</Account>
                                        <Saldo>45,34 PLN</Saldo>
                                    </Bankdata>
                                </Col>
                            </Row>
                        </BankCard>
                    </Col>

                    <Col xs="12">
                        <BankCard>
                            <Row>
                                <Col xs="1">
                                </Col>
                                <Col xs="4">
                                    <BankLogo>
                                        <img alt="" className="img-fluid" src='./assets/millenium.png'></img>
                                    </BankLogo>
                                </Col>
                                <Col xs="6">
                                    <Bankdata>
                                        <BankName>Millenium</BankName>
                                        <Account>12 3245 3245 4356</Account>
                                        <Saldo>3 456 PLN</Saldo>
                                    </Bankdata>
                                </Col>
                            </Row>
                        </BankCard>
                    </Col>
                    <Col>
                        <Butt>
                        ZAPŁAĆ TELEFONEM
                        </Butt>
                    </Col>
                </Row>
            </Container>
        </AppCard>
    )
}

export { Card }

const Butt=styled.div`

margin-top:20px;
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
text-align: center;
color: #FFFFFF;
background: #21B630;
font-size: 4vw;
text-align:center;
padding-top:10px;
padding-bottom:10px;
box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.8);
`

const BankCard = styled.div`
margin:10px;
background: #FDF7F7;
box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.8);
`
const BankName = styled.h3`

font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
font-size: 7vw;
text-align: center;
color: #000000;


`
const Account = styled.h4`
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
font-size: 3vw;
text-align: center;

color: #878787;


`
const Saldo = styled.h3`
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
font-size: 5vw;
text-align: center;

color: #21B630;
`

const BankLogo = styled.div`
margin-top:10px;
`

const Bankdata = styled.div`
margin:10px;
`



const AppCard = styled.div`
    text-align: center;
    font-size: 1rem;
    margin: 20vh 2.5vw 0;
    height: 70vh;
    position: absolute;
    width: 95vw;
    background: #FBFAFA;
    border: 1px solid #000000;
    box-sizing: border-box;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
`