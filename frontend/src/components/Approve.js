import React from 'react'
import styled from 'styled-components';
import { Col, Row, Container } from 'reactstrap';

import { Link } from 'react-router-dom'

function Approve(props) {

    var value = props.value;
    var name = props.name;
    var description = props.description;

    return (
        <>

            <Container>
                <Row>
                    <Back>
                        <Col xs="1"></Col>
                        <Col md="10">
                            <br />
                            <br />
                            <Line>
                                {description || 'Bilet do Krakowa'}
                            </Line>

                            <Line>
                                {name || 'Jan Nowak'}
                            </Line>

                            <Line>
                                {value || 15.30} PLN
                            </Line>

                            <Butt to="card">
                                ZAPŁAĆ
                            </Butt>

                            <Butt2 to="card">
                                ODRZUĆ
                            </Butt2>
                            <br />
                            <br />
                        </Col>
                    </Back>
                </Row>
            </Container>

        </>
    );

}

export { Approve }

const Butt2 = styled(Link)`


margin-top:20px;
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
text-align: center;
color: #FFFFFF;
background: #c62828;
font-size: 4vw;
text-align:center;
padding-top:10px;
padding-bottom:10px;
box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.8);

display:block;
text-ornaments:none;

::hover {
    cursor: pointer;
}



`
const Butt = styled(Link)`

margin-top:20px;
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
text-align: center;
color: #FFFFFF;
background: #21B630;
font-size: 4vw;
text-align:center;
padding-top:10px;
padding-bottom:10px;
box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.8);

display:block;
text-ornaments:none;

::hover {
    cursor: pointer;
}


`
const Line = styled.div`
background: #DDDDDD;
box-shadow: 0px 4px 5px rgba(0, 0, 0, 0.8);
font-family: Roboto Condensed;
font-style: normal;
font-weight: normal;
font-size: 20px;
text-align: center;
margin-bottom:40px;
padding-bottom:20px;
padding-top:20px;
color: #000000;
`

const Back = styled.div`
background-color: #fefbd8;
margin:10px;
width:100%;
margin-top:30px;
margin-bottom:30px;
padding:10px;
`